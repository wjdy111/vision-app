import request from "@/utils/request.js"
import api from "@/utils/api.js"
export const validateText = (content) => {
	return new Promise((resolve, reject) => {
		var invalidWords = ["fuck", "tmd", "他妈的", "黄色网站", "黄色视频", "黄色录像", "黄图", "赌博", "互裸", "裸体", "裸聊",
		"成人网站","成人视频","成人聊","幼童", "暴露", "暴力","低俗","恶俗","艳俗","约炮","下贱","恶心","色情","走光","死光","死绝","激凸",
		"丝袜","大胸","裸胸","裸贷","美眉","附近美","一日情","一夜情","色色","啪啪","宾馆","线下约","一炮","打炮","枪战","逼","逼逼","奸情","迷药","+v","+微"]
		var isVaild = true
		var msg = ""
		for (let i = 0; i < invalidWords.length; i++) {
			if (content.indexOf(invalidWords[i]) !== -1) {
				isVaild = false
				msg = invalidWords[i]
				break
			}
		}
		
		if (!isVaild) {
			reject(msg)
		} else {
			request.post(api.checkText,{
			  "images": "",
			  "text": content
			}).then(res => {
				console.log(res, 'resqq')
				if (res.code == 200) {
					resolve()
				} else {
					reject(res.message)
				}
			}).catch(err => {
				console.log(err, 'resqqerr')
				reject(err.data.message)
			})
		}
	})
}

export const validateImagesrc = (src) => {
	return new Promise((resolve, reject) => {
		request.post(api.checkImage,{
		  "images": src,
		  "text": ""
		}).then(res => {
			if (res.code == 200) {
				resolve("success")
			} else {
				reject(res.message)
			}
		}).catch(err => {
			reject(err.data.message)
		})
	})
}

export const validateImage = (data) => {
	return new Promise((resolve, reject) => {
		uni.uploadFile({
			url: api.checkImage,
			filePath: data,
			name: 'file',
			header: {
				"X-Access-Token": "111"
			},
			formData: {
				text: ''
			},
			success: (res) => {
				console.log(res)
				if (res.data.code == 200) {
					resolve(res.data)
					//uni.hideLoading()
				} else {
					reject(res.message)
				}
			},
			fail: err => {
				reject(err)
			}
		});
		
	})
}