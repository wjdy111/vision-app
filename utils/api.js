//const baseUrl = "http://120.24.23.155:9090/jeecg-boot"
// const baseUrl = "https://www.caoss.vip/dcck/jeecg-boot"
//dcck2  是正式服务器  dcck是测试服务器
// const baseUrl = "https://www.visionfamily.com.cn/dcck2"
// const baseUrl = "http://120.24.23.155:9090/jeecg-boot"
const baseUrl = "http://106.15.72.68:9091/jeecg-boot"
// DEV const signInTempId = 'PKkYp81pkgpu_x3AZUlDv9j_ch_aYAwkGjeumy56Pr0'
const signInTempId = '6CRN8Jhg9wWq3WlRgMe2VppAe5La3HG_K96bBq5p0lA'
const api = {
	baseUrl:baseUrl,
	signInTempId: signInTempId,
	uploadImage:baseUrl+"/sys/common/uploads",
	getDic: baseUrl + "/sys/api/queryDictItemsByCode", //获取字典
	getOpenId: baseUrl + "/wx/auth/loginWeixin", //获取openid
	getUserInfo: baseUrl + "/wx/auth/login_by_weixin", //获取登录信息
	editUserInfo: baseUrl + "/dcck/dcckUser/edit", //编辑用户信息
	getUserById: baseUrl + "/dcck/dcckUser/queryById", //查询用户信息
	getBanner:baseUrl+"/dcck/dcckBanner/appletList",//banner图
	getPostsDetail:baseUrl+"/dcck/dcckBanners/queryByIds",//海报图
	getNewsList:baseUrl+"/dcck/dcckConsultingService/appletList",//获取各种资讯
	getNewsDetail:baseUrl+"/dcck/dcckConsultingService/queryByIdApplet",//资讯详情
	getNewsDetail2:baseUrl+"/dcck/dcckConsultingService/queryByIds",//资讯详情
	getNewsColumns:baseUrl+"/dcck/dcckClassification/list",//资讯分类
	doStar:baseUrl+"/dcck/dcckConsultingService/queryByIdAppletFunction",//点赞收藏
	cancelStar:baseUrl+"/dcck/dcckConsultingService/cancelByIdAppletFunction",//取消点赞收藏
	myStar:baseUrl+"/dcck/dcckUserCollection/appList",//我的点赞收藏发帖
	myRecords:baseUrl+"/dcck/dcckUserBrowseRecords/appList",//我的记录
	checkSignToday:baseUrl+"/dcck/dcckSign/checkUser",//检查今天是否签到
	doSign:baseUrl+"/dcck/dcckSign/add",//签到
	mySignInfo:baseUrl+"/dcck/dcckSign/userSign",//我的签到信息
	posts:baseUrl+"/dcck/dcckPosts/appList",//社区帖子
	postDetail:baseUrl+"/dcck/dcckPosts/queryByIdApp",//帖子详情
	deletePost:baseUrl+"/dcck/dcckPosts/delete",//通过ID删除帖子
	republishPost:baseUrl+"/dcck/dcckPosts/add",//发帖
	columnsList:baseUrl+"/dcck/dcckColumn/list",//帖子栏目
	topicList:baseUrl+"/dcck/dcckTopicConversation/list",//话题列表
	modelList:baseUrl+"/dcck/dcckColumn/appList",//帖子模块
	thumbPost:baseUrl+"/dcck/dcckPosts/thumbsUp",//帖子点赞
	thumbPostNo:baseUrl+"/dcck/dcckPosts/thumbsUpNo",//帖子取消点赞
	collectionPost:baseUrl+"/dcck/dcckPosts/collection",//帖子收藏
	collectionPostNo:baseUrl+"/dcck/dcckPosts/collectionNo",//帖子取消收藏
	doReply:baseUrl+"/dcck/dcckReply/add",//添加评论
	thumbsReply:baseUrl+"/dcck/dcckReply/thumbsUp",//点赞帖子的评论
	thumbsReplyNo:baseUrl+"/dcck/dcckReply/thumbsUpNo",//取消点赞帖子的评论
	hotList:baseUrl+"/dcck/dcckCurriculum/hotList",//热门推荐列表
	studyColumnList:baseUrl+"/dcck/dcckCourseClassification/list",//
	studyList:baseUrl+"/dcck/dcckCurriculum/appList",//
	studyDetail:baseUrl+"/dcck/dcckCurriculumDeail/list",//视频分级
	videoCount:baseUrl+"/dcck/dcckCurriculumDeail/queryByIds",//阅读量增加
	videoCountList:baseUrl+"/dcck/dcckCurriculum/queryByIdApp",//阅读量增加
	doExchange:baseUrl+"/dcck/dcckUserExchange/add",//积分兑换
	myExchange:baseUrl+"/dcck/dcckUserExchange/applist",//积分兑换记录
	sendEmail:baseUrl+"/sys/api/sendEmailMsg",//发送邮箱
	sendEmailFile:baseUrl+"/sys/api/sendEmailMsgs",//发送邮箱文件
	myPosts:baseUrl+"/dcck/dcckPosts/appMeList",//我的帖子
	doReport:baseUrl+"/dcck/dcckProblemFeedback/add",//问题反馈
	liveList:baseUrl+"/dcck/dcckLive/userlist",//直播列表
	doLive:baseUrl+"/dcck/dcckLiveUser/add",//直播预约
	liveDetail:baseUrl+"/dcck/dcckLive/queryById",//直播详情
	onTrialList:baseUrl+"/dcck/dcckOnTrial/list",//试用列表
	userTask:baseUrl+"/dcck/dcckTaskConfiguration/queryByUser",//用户完成任务
	inviteList:baseUrl+"/dcck/dcckUserInvitation/applist",//邀请列表
	commentList:baseUrl+"/dcck/dcckReply/queryByMeId",//我的评论
	queryByPostReply:baseUrl+"/dcck/dcckReply/queryByPostReply",//帖子分页
	shareForward:baseUrl+"/wx/auth/share_forward",//分享积分
	saveUserSign:baseUrl+"/dcck/dcckUserSign/add",//用户订阅签到记录
	searchIndex:baseUrl+"/wx/auth/list",//首页搜索
	goodsList:baseUrl+"/dcck/dcckGoods/appList",//商品列表
	goodsDetail:baseUrl+"/dcck/dcckGoods/queryByAppId", // 商品详情 
	integralDetail:baseUrl+'/dcck/dcckUserTask/invitationList',//积分详情
	defaultAddress:baseUrl+'/dcck/dcckAddress/queryByUserIsDefault',//默认收货地址
	buyGoods:baseUrl+'/dcck/dcckUserExchange/addGoods', // 购买商品
	orderDetail:baseUrl+'/dcck/dcckOrder/queryById' ,// 兑换订单详情
	addAddress:baseUrl+'/dcck/dcckAddress/add', // 添加地址
	editAddress:baseUrl+'/dcck/dcckAddress/edit', // 添加地址
	addressList:baseUrl+'/dcck/dcckAddress/list' ,// 查询地址，
	classiFication:baseUrl+'/dcck/dcckDataClassification/appList',//资料分类
	videoGetintegral:baseUrl+'/dcck/dcckCurriculumDeail/queryByIdsVideo',//看视频得积分
	videoAddIntegral:baseUrl+'/dcck/dcckCurriculumDeail/queryByIdsVideo',//看视频得积分 new
	replyDetail:baseUrl+'/dcck/dcckReply/queryByMeReply',//评论消息
	shareOrder:baseUrl+'/wx/auth/share_order', //分享订单加积分
	replyCount: baseUrl+'/dcck/dcckReply/listCount', // 取我收到的评论数
	signupCollect:baseUrl+'/dcck/dcckRegistrationArticle/queryByIdAppletFunction' ,//报名管理收藏
	signupList:baseUrl+'/dcck/dcckRegistrationArticle/list', // 报名列表
	signupAdd:baseUrl+'/dcck/dcckSignUp/add', // 新增报名
	signedList:baseUrl+'/dcck/dcckSignUp/list', // 查询用户报名
	signupDetail:baseUrl+'/dcck/dcckRegistrationArticle/queryById', // 报名详情
	courseCancelCollection: baseUrl+'/dcck/dcckCurriculum/cancelByIdAppletFunction', // 取消收藏课程
	courseCollection: baseUrl+'/dcck/dcckCurriculum/queryByIdAppletFunction', // 收藏课程
	checkText: baseUrl+'/wx/auth/checkText', // 检验文本
	checkImage: baseUrl+'/wx/auth/checkImage' // 检验图像
}
module.exports = api;
