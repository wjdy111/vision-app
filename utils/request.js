function request(url, method = 'GET', data = {}, header = {}) {
	return new Promise((resolve, reject) => {
		uni.showLoading({
			title: '加载中',
			mask: true
		})
		if (method === "upload") {
			uni.uploadFile({
				url: url,
				filePath: data,
				name: 'file',
				header: header,
				formData: {
					biz: 'temp'
				},
				success: (res) => {
					if (res.data.code == 200) {
						resolve(res.data)
						//uni.hideLoading()
					} else if (res.data.code == 500 || res.data.code == 401 || res.data.code ==
						405) {
						reject(res.message)
						uni.showToast({
							title: '服务异常，请联系管理员~',
							mask: false,
							duration: 2500,
							icon: "none"
						})
					} else {
						resolve(res.data)
					}
				},
				fail: err => {
					reject(err)
					uni.showToast({
						title: '网络太慢了,一会再试吧~',
						mask: false,
						duration: 2500,
						icon: "none"
					})
				}
			});
		} else {
			uni.request({
				url: url,
				method: method,
				data: data,
				header: header,
				success: res => {
					console.log('res', res)
					if (res.data.code == 200) {
						resolve(res.data)
						//uni.hideLoading()
					} else if (res.data.code == 500 || res.data.code == 401 || res.data.code ==
						405) {
						reject(res)
						uni.showToast({
							title: res.data.message || '服务异常，请联系管理员~',
							mask: false,
							duration: 2500,
							icon: "none"
						})
					} else {
						resolve(res.data)
					}
				},
				fail: err => {
					reject(err)
					uni.showToast({
						title: '网络太慢了,一会再试吧~',
						mask: false,
						duration: 2500,
						icon: "none"
					})
				}
			})
		}
	})
}

request.upload = function(url, file, header) {
	return new Promise((resolve, reject) => {
		// uni.request({
		// 	url: "https://www.caoss.vip/dcck/jeecg-boot/sys/login",
		// 	method: "POST",
		// 	data: {
		// 		"password": "123456",
		// 		"username": "admin"
		// 	},
		// 	header: {
		// 		"X-Access-Token": '111'
		// 	},
		// 	success: (res) => {
		// 		resolve(request(url, 'upload', file, {
		// 			"X-Access-Token": res.data.result.token
		// 		}))
		// 	}
		// })
		resolve(request(url, 'upload', file, {
			"X-Access-Token": "111"
		}))
	})
}
// 封装一个单独的 get 方法
request.get = function(url, data = {}, header) {
	return new Promise((resolve, reject) => {
		// uni.request({
		// 	url: "https://www.caoss.vip/dcck/jeecg-boot/sys/login",
		// 	method: "POST",
		// 	data: {
		// 		"password": "123456",
		// 		"username": "admin"
		// 	},
		// 	header: {
		// 		"X-Access-Token": '111'
		// 	},
		// 	success: (res) => {
		// 		resolve(request(url, 'get', data, {
		// 			"X-Access-Token": res.data.result.token
		// 		}))
		// 	}
		// })
		resolve(request(url, 'get', data, {
			"X-Access-Token": '111'
		}))
	})
}
// 封装一个单独的 delete 方法
request.delete = function(url, data = {}, header) {
	return new Promise((resolve, reject) => {
		resolve(request(url, 'delete', data, {
			"X-Access-Token": '111'
		}))
	})
}
// 封装一个单独的 post 方法
request.post = function(url, data = {}, header) {
	// uni.getStorage({
	// 	key: "token"
	// })
	return new Promise((resolve, reject) => {
		// uni.request({
		// 	url: "https://www.caoss.vip/dcck/jeecg-boot/sys/login",
		// 	method: "POST",
		// 	data: {
		// 		"password": "123456",
		// 		"username": "admin"
		// 	},
		// 	header: {
		// 		"X-Access-Token": '111'
		// 	},
		// 	success: (res) => {
		// 		resolve(request(url, 'post', data, {
		// 			"X-Access-Token": res.data.result.token
		// 		}))
		// 	}
		// })
		resolve(request(url, 'post', data, {
			"X-Access-Token": "111"
		}))
	})

}
export default request
