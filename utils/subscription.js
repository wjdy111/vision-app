function getSubscription(tmplIds, successFnc, failFnc) {
	uni.getSetting({
		withSubscriptions: true, //  这里设置为true,下面才会返回mainSwitch
		success: function(res) {

			// 调起授权界面弹窗
			if (res.subscriptionsSetting.mainSwitch) { // 用户打开了订阅消息总开关
				if (res.subscriptionsSetting.itemSettings !=
					null) { // 用户同意总是保持是否推送消息的选择, 这里表示以后不会再拉起推送消息的授权
					let moIdState = res.subscriptionsSetting.itemSettings[tmplIds]; // 用户同意的消息模板id
					if (moIdState === 'accept') {
						console.log('接受了消息推送');
						if (successFnc) successFnc(moIdState)

					} else if (moIdState === 'reject') {
						console.log("拒绝消息推送");
						if (failFnc) failFnc(moIdState)

					} else if (moIdState === 'ban') {
						console.log("已被后台封禁");
						if (failFnc) failFnc(moIdState)
					}
				} else {
					// 当用户没有点击 ’总是保持以上选择，不再询问‘  按钮。那每次执到这都会拉起授权弹窗
					uni.showModal({
						title: '提示',
						content: '请授权开通服务通知',
						showCancel: true,
						success: function(ress) {
							if (ress.confirm) {
								wx.requestSubscribeMessage({ // 调起消息订阅界面
									tmplIds: [tmplIds],
									success(res) {
										console.log('订阅消息 成功 ');
										console.log(res);
										if (successFnc) successFnc(res)
									},
									fail(er) {
										console.log("订阅消息 失败 ");
										console.log(er);
										if (failFnc) failFnc(er)
									}
								})

							}
						}
					})
				}

			} else {
				console.log('订阅消息未开启')
			}
		},
		fail: function(error) {
			console.log(error);
		},
	})
}
export default getSubscription;