const validContent = (content) => {
	var invalidWords = ["fuck", "tmd", "他妈的", "黄色网站", "黄色视频", "黄色录像", "黄图", "赌博", "互裸", "裸体", "裸聊",
		"成人网站","成人视频","成人聊","幼童", "暴露", "暴力","低俗","恶俗","艳俗","约炮","下贱","恶心","色情","走光","死光","死绝","激凸",
		"丝袜","大胸","裸胸","裸贷","美眉","附近美","一日情","一夜情","色色","啪啪","宾馆","线下约","一炮","打炮","枪战","逼","逼逼","奸情","迷药","+v","+微"]
	var isVaild = true
	for (let i = 0; i < invalidWords.length; i++) {
		if (content.indexOf(invalidWords[i]) !== -1) {
			isVaild = false
			break
		}
	}
	return isVaild
}
export default validContent