const validPhone =(value)=>{
  if (!(/^1[3456789]\d{9}$/.test(value))){
    return false
  }else{
    return true
  }
}
const validEmail = (value)=>{
	 if (!(/^\w{3,}(\.\w+)*@[A-z0-9]+(\.[A-z]{2,5}){1,2}$/.test(value))){
	   return false
	 }else{
	   return true
	 }
}
module.exports={
	validPhone:validPhone,
	validEmail:validEmail
}