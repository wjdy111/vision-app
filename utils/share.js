export default {
	data() {
		return {
			share: {
				title: 'VisionFamily机器视觉论坛',
				path: '/pages/login/login',
				imageUrl: '',
				desc: '',
				content: ''
			}
		}
	},
	methods: {
		paramsUrl(option, userId) {
			let str = [];
			if (!option.userId && userId) {
				option.userId = userId
			}
			for (let p in option)
				if (option.hasOwnProperty(p)) {
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(option[p]));
				}
			return str.join("&");
		},
		goCustomNavBack() {
			let pages = getCurrentPages();
			if (pages.length < 2) {
				uni.switchTab({
					url: "/pages/index/index"
				})
			}
		},
	},
	onShareTimeline(res){
		let userInfo = uni.getStorageSync("userInfo");
		let pages = getCurrentPages();
		let route = 'path=' + pages[pages.length - 1].route.split("/")[1];
		let optionStr = this.paramsUrl(pages[pages.length - 1].options, userInfo.id);
		let url = "";
		if (pages[pages.length - 1].options.path) {
			url = optionStr
		} else {
			url = route + (optionStr ? ("&" + optionStr) : '')
		}
		return {
			title: this.share.title,
			query: url,
			imageUrl: this.share.imageUrl,
			success: (res) => {
				uni.showToast({
					title: '分享成功'
				})
			},
			fail: (res) => {
				uni.showToast({
					title: '分享失败',
					icon: 'none'
				})
			}
		}
	},
	onShareAppMessage(res) {
		let userInfo = uni.getStorageSync("userInfo");
		let pages = getCurrentPages();
		let route = 'path=' + pages[pages.length - 1].route.split("/")[1];
		let optionStr = this.paramsUrl(pages[pages.length - 1].options, userInfo.id);
		let url = "";
		if (pages[pages.length - 1].options.path) {
			url = optionStr
		} else {
			url = route + (optionStr ? ("&" + optionStr) : '')
		}
		console.log(this.share.path + "?" + url)
		return {
			title: this.share.title,
			path: this.share.path + "?" + url,
			imageUrl: this.share.imageUrl,
			desc: this.share.desc,
			content: this.share.content,
			success: (res) => {
				uni.showToast({
					title: '分享成功'
				})
			},
			fail: (res) => {
				uni.showToast({
					title: '分享失败',
					icon: 'none'
				})
			}
		}
	},
	onLoad: function (options) {
		// wx.hideShareMenu({
		// 	menus: ['shareAppMessage', 'shareTimeline']
		// })
	},
	// if(flag){
	// 	wx.hideShareMenu({})
	// }
	// wx.hideShareMenu({
	//   menus: ['shareAppMessage', 'shareTimeline']
	// })
}
