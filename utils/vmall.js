// getMethod() {
// 	this.$request.get(this.$api.defaultAddress, {
// 		userId: this.userInfo.id
// 	}).then(res => {
// 		// 加载状态重置
// 		uni.hideLoading()
// 		const { success, result } = res
// 		if (success) {
			
// 		}  else {
// 			this.goodsList = []
// 			this.$appFunc.msg('获取积分失败')
// 		}
// 	})
// }
export const getUser = () => {
	return new Promise((resolve, reject) => {
		uni.getStorage({
			key: "userInfo",
			success: (res) => {
				let info = res.data;
				resolve(info)
			},
			fail: () => {
				reject(null)
			}
		})
	})
}