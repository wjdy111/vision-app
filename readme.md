**#开发问题记录**  
1.~~登录授权页需要按钮或者起始页，小程序授权无法静默授权~~  
2.~~tabbar图标没有给全~~  
3.首页轮播图高度建议修改，按照现有高度设置底下内容显示的很少  
4.小程序不支持长按识别二维码  
5.~~签到页没有切图~~  
6.~~热门推荐没有效果图~~  
7.~~领取V+试用资格“已兑”没有切图~~  
8.~~用户详情接口没有生日~~  
9.~~用户没有编号~~  
10.~~收藏提示字段超长~~  
11.~~点赞如何取消~~  
12.~~新闻资讯再加个描述字段，不然页面显示无法确保正常展示~~  
13.话题没有图片  
14.~~视频只有兑换了才返回视频地址~~  
15.~~jpg,jpeg,mp4需要去除token~~  
16.~~播放量加1接口对播放列表提示没有对应数据，对播放二级列表无效~~  
17.发送邮箱没有响应  
18.~~积分兑换导致我的积分没了，需要查看兑换记录~~  
18.~~我的记录没有详情，只有ID~~  
19.~~帖子列表接口没有返回iamges（部分没有返回，部分返回了~~  
20.~~问题反馈提示文字超长~~  
21.~~直播详情没有返回是否预约~~  
22.~~V+试用没有返回是否兑换的状态字段~~  
23.我的兑换记录报错/dcck/dcckUserExchange/list  

**#截止2022年3月21日 16:32:22剩余内容**  
1.搜索跳转页  
2.发布页  
3.直播列表/直播预约/直播详情  
4.我的记录/我的帖子/我的评论/我的收藏  
5.转发及联系客服  
6.<u>接口对接</u>  
7.分享及邀请  

**#截止2022年4月1日 16:59:43剩余内容**  
1.搜索跳转页  
2.直播观看  
3.我的评论/兑换记录  
4.转发及邀请  
5.积分充值  
6.V+试用  

**#截止2022年4月2日 16:29:31剩余内容**  
1.搜索跳转页  
3.我的评论/兑换记录  
4.转发及邀请  

**#截止2022年4月3日 16:57:14剩余内容**  
1.搜索跳转页  
2.我的评论/兑换记录  
3.转发及邀请接口 

**#截止2022年4曰5日 23点54分剩余内容**  
1.首页搜索
2.**问题记录**
share文件中的getStorage只能同步，异步会导致this失效，原因未知


**#优化问题记录**  
1.点击tab滑动到视野内  
2.冒泡事件  

<!-- 30天授权、文档资料 -->
<!-- "miniprogram": {
      "list": [
        {
          "name": "pages/postDetail/postDetail",
          "pathName": "pages/postDetail/postDetail",
          "query": "id=1507993747321323521",
          "scene": null
        },
        {
          "name": "成长",
          "pathName": "pages/growup/growup",
          "query": "",
          "scene": null
        },
        {
          "name": "学习视频",
          "pathName": "pages/studyDetail/studyDetail",
          "query": "id=1508613605037785089",
          "scene": null
        },
        {
          "name": "首页",
          "pathName": "pages/login/login",
          "query": "",
          "scene": null
        },
        {
          "name": "我的",
          "pathName": "pages/my/my",
          "query": "",
          "scene": null
        },
        {
          "name": "帖子",
          "pathName": "pages/communityList/communityList",
          "query": "",
          "scene": null
        },
        {
          "name": "我的帖子",
          "pathName": "pages/myCommunity/myCommunity",
          "query": "",
          "scene": null
        },
        {
          "name": "问题反馈",
          "pathName": "pages/callBack/callBack",
          "query": "",
          "scene": null
        },
        {
          "name": "我的收藏",
          "pathName": "pages/myCollection/myCollection",
          "query": "",
          "scene": null
        },
        {
          "name": "直播列表",
          "pathName": "pages/liveList/liveList",
          "query": "",
          "scene": null
        },
        {
          "name": "直播詳情",
          "pathName": "pages/liveDetail/liveDetail",
          "query": "id=1509703808607412225",
          "scene": null
        },
        {
          "name": "领取试用",
          "pathName": "pages/vOnTrial/vOnTrial",
          "query": "",
          "scene": null
        },
        {
          "name": "我的兑换记录",
          "pathName": "pages/myExchangeList/myExchangeList",
          "query": "",
          "scene": null
        }
      ]
    } -->