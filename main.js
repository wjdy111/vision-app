import App from './App'

// #ifndef VUE3
import Vue from 'vue'
import "@/utils/dateFormat.js";
import request from "@/utils/request.js"
import api from "@/utils/api.js"
import appFunc from "@/utils/appFunc.js"
Vue.prototype.$request = request;
Vue.prototype.$api = api;
Vue.prototype.$appFunc = appFunc;
// main.js
import uView from '@/uni_modules/uview-ui'
import share from '@/utils/share.js'
Vue.mixin(share)
Vue.use(uView)
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif